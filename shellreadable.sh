#!/bin/bash

# a script to unify filenames and directory names as well as make them more usable for the shell. 
# Therefore, spaces and German umlauts, as well as other non-word characters are to be replaced by internationally machine-readable characters. 
# Additionally, uppercase letters are to be substituted by lowercase letters for standardization.


#######################################################
# HELP #
#######################################################

Help()
{
   # Display Help
   echo 	"The script will by default replace upperletters with lowerletters, 
German umlauts with the regular Latin expression and whitespaces with underscores. 
It is possible to define one or two extra arguments containing a single character 
or a sequence. With just one argument the character(s) will be deleted, with two 
arguments the first sequence will be replaced by the second one.
   	
Syntax: shell-readable [-h|-V|-R] [-r] [argument 1] [-s] [argument 2]
   	
options:
-h, --help		Print this Help.
-r [argument 1]		Define an additional character or sequence, wich 
			   1. Will be deleted if no other option is attached 
			   2. Will be replaced by the character/sequence defined with -s.
			   It is not recommended to search for special Regex-characters that must be escaped.
-s [argument 2]		Only works in combination with -r. Will replace the character/sequence
			   defined under -r with the one defined under -s.
-V, --version		Print software version plus license and exit."
   	
}

Version()
{
	# Version and Licence
	echo "shellreadable.sh v.2.0 Copyright (C) 2023  Florian Lukas
Licensed under GNU GPLv3.
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions."
}

#######################################################
# MAIN PROGRAM #
#######################################################

# Set variables. On some Linux-Distributions (e.g. Manjaro or some other Arch-distros) the command "rename" refers to the programm rename.ul
# which is part of the util-linux package and has a different syntax than the here used perl-based rename-command. 
# If this is the case, it has to be checked if the programm "perl-rename" exists to make the script excecutable, because this is the rename-command which is used in
# this script.

if [ -f /usr/bin/perl-rename ]; then
	renamecommand="perl-rename"
elif [ -f /usr/bin/prename ]; then
	renamecommand="rename"
else
	echo 'No perl-based rename-command detected. Please install the Package "perl-rename" (on Arch/Manjaro) 
	or "prename" (Debian/Ubuntu), depending on your Linux-distribution, to use the script.'
	exit 1
fi

#######################################################
# Process the input options. Add options as needed.   #
#######################################################
# Get the options

# Set long-version for special options
for opt in "$@"; do
	shift
	case "$opt" in
		'--help')		set -- "$@" '-h';;
		'--version')	set -- "$@" '-V';;
		*)          	set -- "$@" "$opt"
	esac
done

# Set letters for options
while getopts ":hr:s:V" option; do
   case $option in
      h) # display Help
         Help
         exit;;
      V) # display Version an License
         Version
         exit;;
      r) # needs argument, that defines further sequence to be replaced
         replace=$OPTARG;;
      s) # needs argument, that defines substitution for the extra defined sequence
         substitute=$OPTARG;;
      \?) # Invalid option: if an invalid option is entered, the script shows the help and is aborted without further excecution
         echo
		 echo "Error: Invalid option. See help for solution:"
		 echo
		 Help
         exit 1
   esac
done

###########################################
# Functions for Execution #
###########################################

replace-characters()
{
	$renamecommand 'y/A-Z/a-z/' * # replace uppercase characters with lowercase
	$renamecommand 's/Ä|ä/ae/g' * # replace upper- or lowercase Umalut Ä with ae
	$renamecommand 's/Ö|ö/oe/g' * # same for Ö
	$renamecommand 's/Ü|ü/ue/g' * # same for Ü
	$renamecommand 's/à|á|â|ã/a/g' * # same for accented characters in the following
	$renamecommand 's/è|é|ê|ë/e/g' *
	$renamecommand 's/ì|í|î|ï/i/g' *
	$renamecommand 's/ò|ó|ô|õ/o/g' *
	$renamecommand 's/ù|ú|û/u/g' *
	$renamecommand 's/ý|ÿ/y/g' *
	$renamecommand 's/ñ/n/g' *
	$renamecommand 's/ß|\x{1E9E}|\x{00DF}/ss/g' * # same for ß
	$renamecommand 's/[^\w\.\-]|\.(?!.{2,6}$|.{2,6}\.\w{2,6}$)/_/g' * # replace any non-word-character with an underscore, but save dots of file-extensions (.text etc.) and possible backup file-extensions (.txt.bak). !!!Uncommon file-extensions with more than 6 characters may be deleted!!!
	$renamecommand 's/_{2,}/_/g' * # replace multiple underscores with a single underscore
	echo "All non-word-characters and whitespaces replaced with underscores, German Umlauts replaced with latin characters."
}

delete-or-substitute()
{	
	mapfile -t replacements < <( find -maxdepth 1 -name "*$replace*" )
	# tests if an argument for replacing have been given to the command and if files containing the sequence exist
	if [[ -n "$replace" && ${#replacements[@]} -eq 0 ]]; then 
		echo -e "No sequence \"$replace\" found to replace or delete." # if not, the script is stopped
		exit 0
	elif [[ -n "$replace" && -n "$substitute" ]]; then # condition for replacing a choosen sequence/character with a secon choosen one.
		$renamecommand "s/(?<!\.)$replace/$substitute/g" * #replacing the first sequence with the second one in all files, in which it occured
		echo -e "Every occuring \"$replace\" replaced with \"$substitute\", please check the file extensions for errors" 
	elif [[ -n "$replace" && -z "$substitute" ]]; then # condition for deleting a choosen sequence/character.
	# negative lookbehind (?<!\.) saves the everything following the last dott -> file-extensions.
		$renamecommand "s/(?<!\.)$replace//g" * # deleting the sequence attached to the command from all files, in which it occured
		echo -e "Every occuring \"$replace\" deleted, please check the file extensions for errors"
	fi
}

###########################################
# Execution Process #
###########################################

# puts all files and directories with one or several of the characters to be replaced in the variable $files
mapfile -t files < <( LC_ALL=C find . -maxdepth 1 -name '*[!0-9a-z_.-]*' ) 

# Stops the script if no upperletter, Umlaut or whitespace is found and no further option has been passed to the command
if [[ ${#files[@]} -eq 0 && $# -eq 0 ]]; then 
	echo "No file or directory found to rename"
	exit 0
elif [[ -z "$replace" && -n "$substitute" && ${#files[@]} -eq 0 ]]; then
	echo -e "Substitution not possible. You need to define an -r argument ahead of -s to use substitution"
	exit 2
elif [[ -z "$replace" && -n "$substitute" && ${#files[@]} -gt 0 ]]; then
	echo -e "Substitution not possible, script stopped without renaming anything. You need to define an -r argument ahead of -s to use substitution"
	exit 2
elif [[ ${#files[@]} -gt 0 && -z "$replace" ]]; then # if replacable characters are found and the script is called with no argument, run the standard function
	replace-characters
elif [[ -n "$replace" && ${#files[@]} -gt 0 ]]; then # if replacable characters are found and the script is called with additional -r and -s arguments, run the standard function and afterwards the substitute function
	replace-characters
	echo 
	delete-or-substitute
elif [[ -n "$replace" && ${#files[@]} -eq 0 ]]; then # if no replacable characters are found, but the script was called with additional -r and -s arguments, only run the substitute function
	delete-or-substitute
elif [[ -n "$substitute" && -z "$replace" ]]; then # If a substitution sequence is called without the replace-argument, cancle the script
	echo "No sequence which should be replaced defined. Please use the -r option or view the help using --help option"
	exit 2
fi