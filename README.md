# "shell-readable": a script to standardize names of files and directories

- ["shell-readable": a script to standardize names of files and directories](#shell-readable-a-script-to-standardize-names-of-files-and-directories)
  - [Install](#install)
    - [Download or clone the files](#download-or-clone-the-files)
    - [Copy it to a PATH-directory](#copy-it-to-a-path-directory)
      - [Optional: inlcude directory to $PATH](#optional-inlcude-directory-to-path)
    - [Make the script executable](#make-the-script-executable)
    - [Checking for right renaming-programm](#checking-for-right-renaming-programm)
  - [Usage](#usage)
    - [Standard Usage](#standard-usage)
    - [Extra Substitution](#extra-substitution)


The script can be interpreted as the little command-line-brother of GUI-batch-renamer [GPRename](http://gprename.sourceforge.net/).[^1] 

I have been looking for a fast shell-command to rename a big batch of files for quiet a while. And because i work with the command-line most of the time a GUI-based programm wasn't a real option. So I decided to write my own script; and here it is.

Experienced Unix-developers will certainly find some mistakes or unnecessary commands and/or options. If so, feel free to change and improve the code. For me, right now, the script works fine, but i'm always open to learn new facets and to improve in this way.

## Install

The following installation guide explains how to use the script on a Linux-based system; in my case Manjaro. For Mac it's almost the same procedure as for Linux. There is a possiblity to run Bash-scripts on [Windows](https://www.howtogeek.com/261591/how-to-create-and-run-bash-shell-scripts-on-windows-10/) too. But since I don't use Windows or MacOS myself, I haven't tested the script on either system, so I make absolutely no guarantee that it will run there at all.

To install the script you just need to download the file `shellreadable.sh`, copy it to a directory included in your `$PATH`, and make the script-file excecutable.

### Download or clone the files

To download the file just click on the **code** Button at the main-branch and save the .zip-file.

I recommend cloning the repository to your local machine:

~~~
git clone https://gitlab.com/lukeflo/shell-readable-script.git
~~~

### Copy it to a PATH-directory

After cloning the repository you should copy the script-file to a directory included in your `$Path`. 

I use the directory `~/.local/bin/` which on most Linux-systems is automatically included.

From the directory, you just cloned the Repo to, copy the script-file `shellreadable.sh` to `~/.local/bin/` and change to the same directory:

~~~
cp shellreadable.sh ~/.local/bin && cd ~/.local/bin
~~~

#### Optional: inlcude directory to $PATH

If the directory, where you want to place the script, is not included to your `$PATH`, you have to do it by yourself.

Open the file `~/.profile` with your favorite editor and include the following lines:

~~~bash
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi
~~~

This works to include the directory `~/.local/bin/` to your `$PATH`.

The pathname `"$HOME/.local/bin"` should be replaced by the pathname of the directory you put the script in if you saved it somewhere else.

### Make the script executable

To run the script from the Terminal it has to be executable. Therefore run the following command from the directory that contains the script and is included in `$PATH`:

~~~shell
chmod 555 shellreadable.sh
~~~

Alternatively, the following command can be used if the owner of the file wants to have the rights to change the script-file too:

~~~shell
chmod 755 shellreadable.sh
~~~

### Checking for right renaming-programm

As i recently switched to Manjaro/Arch, i found out that the **rename** command regular used by Ubuntu is just a symlink to the **prename** command in `/usr/bin/prename`.[^2] On Manjaro the regular command **rename** is part of the util-linux package which ist *not* using Perl. Therefore executing `rename` on Ubuntu will launch a different programm than on Manjaro.

Since my script is based on the **rename** command using Perl-syntax with Regular Expressions you need to check your system for the right programm:

* On **Ubuntu/Debian** check if the programm **prename** is installed; for example with `which prename`. The output should be `/usr/bin/prename`.
* On **Manjaro** (or **Arch** depending on your Arch-setup) you might have to install the package **perl-rename** with `sudo pacman -S perl-rename`. Afterwards check with `which perl-rename` or `pacman -Qs perl-rename` if the programm has been succesfully installed.

The script will check for one of the above mentioned programms and choose the right one to execute. If wether **prename**, nor **perl-rename** is installed, it will print a warning and cancel the script.

I myself haven't checked it on other Linux-distros, so no guarantee that it will work there.

## Usage

### Standard Usage

The usage is really simple. Go to the directory which includes the files and folders you want to rename and run the script with the `shellreadable.sh` command.

If there were files and folders with names that included non-word-characters, whitespaces, uppercase-characters or German Umlauts, the script substitutes them with underscores, lowercase or the standard latin characters:

| Character | Replacement |
|---|---|
| Ä/ä | ae |
| Ö/ö | oe |
| Ü/ü | ue |
| à, è, ì, ò, ù etc. | particular regular character |
| [A-Z] | [a-z] |
| ?=!§$ etc. | _ |

Furthermore, it will replace resulting underscores in a row with a single underscore. Hyphens as well as dots (**.**) of file extensions (e.g. `.txt`, `.html`, `.mp4` etc.) or backuped files (e.g. `.bib.bak`) are excluded from this substitution. The latter works only for file-extensions with a maximum of 6 characters. **If you have files with some unusual long file-extensions, I highly recommend not to use the script in the respective folder**.

**Moreover, this program comes with absolutely no warranty and should not be run in any *root* directory.** I gave my best to make it as save as possible, and on my system I use it a lot inside my `/home` directory without any serious problems. But if it brakes some files on your system, i can't help you!

### Extra Substitution

The script offers you the possibility to substitute self-defined strings. You can use the `-r` option followed by a string:

1. to delete the respective string, or
2. to substitute the respective string with another string defined by the `-s` option.

For example, the following command would replace the string **hello** with the string **goodbye**:

```shell
$ shellreadable -r hello -s goodbye
```

As a result, a file named `hello-world.txt` would be renamed into `goodbye-world.txt`.

The self-defined deletion or substitution always takes place **after** the regular substitution mentioned [above](#standard-usage). So it would make no sense to define a substitution containing German Umlauts, because the script would have substituted them before it runs the specific `delete-or-substitute` function.

Furthermore, i recommend to use only standard latin word-characters and digits for self-defined substitutions. Such with special characters might cause some trouble, because the special characters can't be escaped properly.


---

[^1]: There is no connection between my script, the GPRename-Tool, and its creators. I encountered this programm only when my script task was already over. Just wanted to mention it, because it seems to be a GUI-based equivalent. And many people, who are afraid of using the command-line, could be willing to try the GUI-tool.

[^2]: See e.g. https://wiki.ubuntuusers.de/rename/#Installation